# 答题卡图片识别学习

#### Description
penCV是一个基于BSD许可（开源）发行的跨平台计算机视觉库，它提供了一系列图像处理和计算机视觉方面很多通用算法。是研究图像处理技术的一个很不错的工具。最初开始接触是2016年因为公司项目需要，但是当时网上可供参考的demo实在太少了，而且基本上都是基于C、C++实现的。也就是从2017年开始，关于java+opencv的资料才渐渐多起来。处于这种情况，就想搭建一个有助于我们学习和了解opencv的一个平台。因此就有了这个系统。

#### Software Architecture
Software architecture description

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)